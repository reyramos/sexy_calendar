angular.module('app')
.factory
( 'templateService'
, [
	  '$templateCache'
	, function
		( $templateCache
		){
			var service = {};
			
			$templateCache.put('template/datepicker/datepicker.html',
					 '<div class="calendar-panel">'
						+'<div class="panel-heading">'
							+'<div ng-click="move(-1)" class="thead move-cal"><span class="icon-leftarrow"></span></div>'
							+'<div class="thead cal-title" data-ng-bind="title"></div>'
							+'<div ng-click="move(1)" class="thead move-cal"><span class="icon-rightarrow"></span></div>'
						+'</div>'
						+'<div class="panel-body">'
						+'<div class="cal-row calendar-week-names">'
							+'<div class="cal-cell" data-ng-repeat="label in labels" data-ng-bind="label"></div>'
						+'</div>'
						+'<div class="tbody">'
							+'<div class="cal-row" data-ng-repeat="row in rows">'
								+'<div class="cal-cell" ng-repeat="dt in row" ng-class="{\'selected\': dt.isSelected, \'disabled\':dt.disabled}">'
									+'<span class="size-contain">&nbsp;'
						 				+'<span class="cal-btn-contain">'
										+'<span class="flag"></span>'
										+'<button class="cal-btn" ng-click="select(dt.date)" ng-class="{\'selected\': dt.isSelected, \'date-disabled\':dt.disabled}" ng-disabled="dt.disabled"><span class="label" ng-class="{muted: ! dt.isCurrent}" ng-bind="dt.label"></span></button>'
						 				+'</span>'
						 			+'</span>' //size contain
								+'</div>'
							+'</div>'
						+'</div>'
						+'</div>'

					+'</div>'//calendar-panel

			)
			
			return service;
		 }
  ]
)

